﻿#ifndef OUTPUTWIDGET_H
#define OUTPUTWIDGET_H

#include <QTableWidget>

class OutputWidget : public QTableWidget
{
    Q_OBJECT
public:
    explicit OutputWidget(QWidget *parent = nullptr);

private slots:
    void onCurrentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);
};

#endif // OUTPUTWIDGET_H
