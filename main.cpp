﻿#include "MainWindow.h"
#include <QApplication>

#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    //加载QSS
    a.setStyle(QStyleFactory::create("fusion"));
    a.setStyleSheet("file:///:/qss/outputwidget.qss");

    return a.exec();
}
