﻿#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle(QStringLiteral("日志输出窗口Demo by.Qt小罗"));

    ui->tableWidget->horizontalHeader()->resizeSection(1, 250);
}

MainWindow::~MainWindow()
{
    delete ui;
}
