﻿#include "OutputWidget.h"

#include <QDebug>
#include <QHeaderView>

#define DEFAULT_SECTION_SIZE (18)

OutputWidget::OutputWidget(QWidget *parent) : QTableWidget(parent)
{
    setShowGrid(false);
    setFocusPolicy(Qt::NoFocus);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);

    verticalHeader()->setDefaultSectionSize(DEFAULT_SECTION_SIZE);

    QHeaderView *header = horizontalHeader();
    header->setHighlightSections(false);
    connect(header, &QHeaderView::sectionResized, [=]() {
        QTableWidgetItem *currentItem = this->currentItem();
        if (!currentItem)
        {
            return;
        }
        resizeRowToContents(this->row(currentItem));
    });

    connect(this, &OutputWidget::currentItemChanged, this, &OutputWidget::onCurrentItemChanged);
    connect(this, &OutputWidget::itemPressed, this, [=](QTableWidgetItem *item) {
        setCurrentItem(item);
    });
}

void OutputWidget::onCurrentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{
    if (previous)
    {
        setRowHeight(this->row(previous), DEFAULT_SECTION_SIZE);
    }

    resizeRowToContents(this->row(current));
}
